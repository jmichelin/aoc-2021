# Warts an all

# list_of_depths = [
#   1,
#   2,
#   3,
#   4,
#   5,
#   4,
#   3,
#   2,
#   1,
#   2,
#   3,
#   4,
#   3
# ]


list_of_depths = open('advent_day_one.txt', 'r')
list_of_depths = list_of_depths.readlines()

# Like this Curtis, he says yes
# with open('advent_day_one.txt', 'r') as list_of_depths:
#     list_of_depths = open('advent_day_one.txt', 'r')
# list_of_depths = list_of_depths.readlines()


# print(list_of_depths.read())

search_depth = list_of_depths[0]
depth_increase_counter = 0

# for idx, depth in enumerate(list_of_depths):
#   # print(idx, depth)
#   if int(depth) > int(search_depth):
#     depth_increase_counter += 1
#   search_depth = depth
# print(depth_increase_counter)

for idx, depth in enumerate(list_of_depths):
  try:
    window_one = list_of_depths[idx] + list_of_depths[idx+1] + list_of_depths[idx+2]
    window_two = list_of_depths[idx+1] + list_of_depths[idx+2] + list_of_depths[idx+3]
    if window_two > window_one:
      depth_increase_counter += 1
    # print('test', idx)
    # print(window_one)
  except IndexError:
    pass
  continue

# Another Attempt 1523
depth_increase_counter = sum(list_of_depths[i] > list_of_depths[i-3] for i in range(3, len(list_of_depths)))

print(depth_increase_counter)
# A = values[index] + values[index+1] + values[index+2]
# B = values[index+1] + values[index+2] + values[index+3]
# If B > A 
#   depth_increase +=1