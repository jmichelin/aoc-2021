# with open('advent_day_two.txt', 'r') as list_of_moves:
#     list_of_moves = open('advent_day_one.txt', 'r')
# list_of_moves = list_of_moves.readlines()

vertical, forward = 0, 0
with open('advent_day_two.txt', 'r') as list_of_moves:
    for move in list_of_moves.readlines():
        cmd, val = move.split()
        if cmd == "forward": forward   += int(val)
        elif cmd == "down":  vertical += int(val)
        elif cmd == "up":    vertical -= int(val)
print(vertical * forward)

vertical, forward, aim = 0, 0, 0
with open('advent_day_two.txt', 'r') as list_of_moves:
    for move in list_of_moves.readlines():
        cmd, val = move.split()
        if cmd == "forward":
                             vertical += aim * int(val)
                             forward   += int(val)
        elif cmd == "down":  aim   += int(val)
        elif cmd == "up":    aim   -= int(val)
print(vertical * forward)